# Futurama Quotes

Use this shell script to display a random quote from Futurama.

	$ ./futurama-quote

	  Amy: Look at us, living like trash eating bums in an alley now.
	  Zoidberg: Yes, now...

It uses [ANSI escape codes](https://en.wikipedia.org/wiki/ANSI_escape_code) to colorise the output.

## Installation

To download and compile the quotes data from the Infosphere, you will need
these programs:

- [`pandoc`](https://archlinux.org/packages/core/x86_64/pandoc/)
- [`awk`](https://archlinux.org/packages/core/x86_64/gawk/)
- [`sed`](https://archlinux.org/packages/core/x86_64/sed/)
- [`curl`](https://archlinux.org/packages/core/x86_64/curl/)
- [`wget`](https://archlinux.org/packages/core/x86_64/wget/)
- [Linux core utilities](https://archlinux.org/packages/core/x86_64/coreutils/)

Download and format quotes from all of the Infosphere with:

	$ make

To install to `/usr/local/bin/futurama-quote` and
`/usr/local/share/futurama-quote/`, run as super-user:

	# make install

To display a quote when starting bash, you can add this line to `~/.bashrc`:

	command -v futurama-quote > /dev/null && futurama-quote || :

The call to `command` ensures that no error is thrown when the command
`futurama-quote` cannot be found.

## Usage

- `futurama-quote`: display a random Futurama quote
- `futurama-quote zapp`: display a random quote containing the word "zapp", using case-insensitive `grep`
- `futurama-quote zapp 10`: display 10 matching quotes

## License

This project is licensed under [Creative Commons Attribution-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-sa/3.0/).

It uses data from [The Infosphere](https://theinfosphere.org/), the Futurama memory bank that anyone can edit.
