all: test stats

test: futurama-quote quotes.txt
	FUTURAMA_QUOTES=quotes.txt ./$<

stats: quotes.txt
	@echo 'source:  The Infosphere (https://theinfosphere.org/)'
	@printf 'number of scanned articles: '
	@wc -l links.txt | cut -d' ' -f1
	@printf 'number of unique quotes: '
	@wc -l $< | cut -d' ' -f1
	@printf 'characters with the most quotes:\n'
	@tr '\t' '\n' < $< | sed '/^[^{]/d;s/^{\([^}]*\)}:.*$$/\1/;s/the //i;s/^\(Prof\.\?\(essor\)\?\)\? \?\(Farnsworth\)\?$$/Farnsworth/i;s/\(Dr.|Doctor\) Zoidberg/Zoidberg/i;s/Zapp Brannigan/Zapp/' | sort | uniq -c | sort -n -r | sed 20q

install:
	install -Dm755 futurama-quote -t "$(DESTDIR)/usr/local/bin/"
	install -Dm644 quotes.txt -t "$(DESTDIR)/usr/local/share/futurama-quote/"
	install -Dm644 "LICENSE" -t "$(DESTDIR)/usr/local/share/licenses/futurama-quote/"

uninstall:
	rm -f "$(DESTDIR)/usr/local/bin/futurama-quote"
	rm -rf "$(DESTDIR)/usr/local/share/futurama-quote"
	rm -rf "$(DESTDIR)/usr/local/share/licenses/futurama-quote"

quotes.txt: quotes.html filter.lua
	pandoc -L filter.lua -t plain --wrap=none -i $< \
		| tr '\n' '\t' \
		| sed 's/\t\t/\n/g' \
		| sed '/AL1/d;/AL2/d;/^$$/d' \
		| sort | uniq > $@

QUOTESSTART=<ul class="quotes">
QUOTESEND=<\/ul>
quotes.html: exports.html
	sed 's/${QUOTESSTART}/\n${QUOTESSTART}\n/g;s/${QUOTESEND}/\n${QUOTESEND}\n/g' $< \
	| awk '/^${QUOTESSTART}$$/{flag=1;next}/^${QUOTESEND}$$/{flag=0}flag' > $@

PAGEURL:=https://theinfosphere.org/$${pagename}
exports.html: links.txt
	while read pagename ; do \
		echo "Downloading '$${pagename}'" 1>&2; \
		curl -s "${PAGEURL}" \
		| sed "s|{{PAGENAME}}|$${pagename}|" ; \
	done < $< > $@

links.txt: links.html
	sed 's/href=/\nhref=/g' $< \
	| grep -o '^href=".*"' \
	| cut -d '"' -f 2 \
	| sort \
	| sed 's!^/!!' \
	| sed '/^Commentary:/d' \
	> $@

PATTERNSTART=mw-search-results
PATTERNEND=printfooter
links.html: search.html
	sed -n '/${PATTERNSTART}/,/${PATTERNEND}/p' $< > $@

LIMIT=10000
SEARCH=%%3D%%3D+quotes+%%3D%%3D
SEARCHURL:=https://theinfosphere.org/index.php?title=Special:Search&limit=${LIMIT}&search=${SEARCH}
search.html:
	wget -O $@ '${SEARCHURL}'

clean:
	rm -rf search.html links.html links.txt exports.html quotes.html quotes.txt

.PHONY: all stats test install uninstall clean
