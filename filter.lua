function Link(e)
	return e.content
end

function Div(e)
	return e.content
end

function Superscript(e)
	return {}
end

function Strong(e)
	return pandoc.Str("{"..pandoc.utils.stringify(e).."}")
end

function Emph(e)
	return pandoc.Str("<"..pandoc.utils.stringify(e)..">")
end
